import { Schema, model, Document, PopulatedDoc} from 'mongoose';

interface PersonSchema {
    story: PopulatedDoc<StorySchema & Document>,
    name: string,
    age: number
}
interface StorySchema {
  name: String,
  title: string,
}
const storySchema = new Schema<StorySchema>({
  name: String,
  title: String
});

const personSchema = new Schema<PersonSchema>({
  story: { type: Schema.Types.ObjectId, ref: 'Stories' },
  name: String,
  age: Number,
});

const Story = model<StorySchema>('Stories', storySchema);
const Person = model<PersonSchema>('Person', personSchema);


export { Story, Person, PersonSchema, StorySchema }