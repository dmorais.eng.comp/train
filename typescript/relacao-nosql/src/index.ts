import { connect, connection } from "mongoose"
import { Story, Person, PersonSchema, StorySchema } from "./models/exemples"
connect('mongodb+srv://ommed:0mm3d@node0.wk3go.mongodb.net/ms-user-exemple?retryWrites=true&w=majority',
        { useNewUrlParser: true, 
          useUnifiedTopology: true
        }
)

connection.on('error', console.error.bind(console, 'connection error:'));
connection.once('open',async function() {
    try {
        const story = new Story ({
            name: 'Diozefe',
            title: 'Algo diferente'
        })
        
        const person = new Person({
            name: 'Diozefe Morais',
            age: 23
        })
        story.save(async function(err, story){
            if (err) throw err;
            person.story=story._id
            await Story.create(story)
        })
        person.save( async function(err, per){
            if(err) throw err;
            await Person.create(per)
            Person.findOne({}).populate('story').then((doc) => {
                // `doc` doesn't have type information that `child` is populated
                return console.log(doc);
            });
        })
        
          
        // You can use a function signature to make type checking more strict.
    } catch (error) {
        console.log(error.message)
    }

})


