//Questão 1
//let A = [4, 2, 1];
//let B = [2, 5, 3];
//quantidade de frontend
//let F = 2
//Total de empregados
//let N = 3;


//Questão 2
//let A = [7, 1, 4, 4]
//let B =  [5, 3, 4, 3]
//quantidade de frontend
//let F = 2
//Total de empregados
//let N = 4;


//Questão 3
//let A = [5, 5, 5]
//let B = [5, 5, 5]
//quantidade de frontend
//let F = 1
//Total de empregados
//let N = 3;

//Caso Extra
let A = [6, 5, 5, 8, 15, 1, 4, 9]
let B = [5, 5, 5, 4, 2, 7, 5, 20]
//quantidade de frontend
let F = 2
//Total de empregados
//resposta esperada por Igor Andrey 58
//mas o resultado é 43
// Caso 4 arrayA = new int[] { 6, 5, 5, 8, 15, 1, 4, 9 }; arrayB = new int[] { 5, 5, 5, 4, 2, 7, 5, 20 }; FDevs = 2;

function solucao(A, B, F){
    let N = B.length;
    let BC = N-F;
    let frontend, soma, backend=0
    if(F && F>=2 && F<3){
        //front-end
        frontend = parseInt(A[F-F]+A[A.length-1])
        //back-end
        for(let i=0;i<BC;i++){
            backend = backend + parseInt(B[i+1])
        }
        soma= frontend+backend
    }
    if(F && F<2){
        //front-end
        frontend = parseInt(A[F-F])
        //back-end
        for(let i=0;i<BC;i++){
            backend = backend + parseInt(B[i+1])
        }
        soma= frontend+backend
    }
    
    return soma
}
function solucaoAlt(A, B, F){
    let arrayAB=[]
    let soma=0
    let contaA=0
    for (let i = 0; i < A.length; i++) {
        if(A[i] > B[i] && contaA<2){
            arrayAB.push(A[i])
            contaA++
        }
        if(A[i]<B[i]){
            arrayAB.push(B[i])
        }
        if(A[i]==B[i] ){
            arrayAB.push(B[i])
        }
    }
    for(let i=0;i<arrayAB.length;i++){
        soma=soma+arrayAB[i]
    }
    return soma
}

console.log('A soma máxima de contribuições é: '+solucaoAlt(A, B, F))