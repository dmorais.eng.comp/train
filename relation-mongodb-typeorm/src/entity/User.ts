
import {Entity, ObjectIdColumn, ObjectID, Column, CollationDocument } from "typeorm";

@Entity()
export class User {

    @ObjectIdColumn()
    id?: ObjectID;

    @Column()
    nome: string;

    @Column()
    idade: number;

    @Column()
    posts?: [
        {
            id:ObjectID, ref?: 'post'
        }
    ];

}
