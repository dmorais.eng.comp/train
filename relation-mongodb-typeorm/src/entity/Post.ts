
import {Entity, ObjectIdColumn, ObjectID, Column, } from "typeorm";


@Entity()
export class Post {

    @ObjectIdColumn()
    id?: ObjectID;

    @Column()
    titulo: string;

    @Column()
    subtitulo: string;

    @Column()
    user:
        {
           id: ObjectID, ref?: 'user'
        };

}