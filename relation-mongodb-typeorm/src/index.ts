import "reflect-metadata";
import {createConnection} from "typeorm";
import { Post } from "./entity/Post";
import {User} from "./entity/User";

createConnection().then(async connection => {
    const user = new User();
      user.nome = "Diozefe";
      user.idade = 23
      const userCreated = await connection.manager.save(user);
      console.log(userCreated.id)
    const post = new Post();
      post.titulo='Desenvolvimento';
      post.subtitulo='Testando solução de relação';
      post.user.id=userCreated.id
    
    const postCreated = await connection.manager.save(post);
      const relation = {
        id:postCreated.id
      }
      console.log(postCreated.id)

    userCreated.posts.push(relation)
    await connection.manager.save(userCreated)

    const userAggregate = connection.getMongoRepository<User>(User).aggregate([
          {
            $lookup: {
              from: 'users',
              localField: 'users._id',
              foreignField: 'posts',
              as: 'posts',
            },
          },
    ])
    console.log(userAggregate)

}).catch(error => console.log(error));
