const mongoose = require('../config/database')
const Schema = mongoose.Schema
const userSchema = new Schema({
    nome: String,
    idade: Number,
    posts: [{
        type: mongoose.Schema.Types.ObjectId, ref: 'Post'
    }]
},{
    timestamps:true
})

module.exports = mongoose.model('User', userSchema);