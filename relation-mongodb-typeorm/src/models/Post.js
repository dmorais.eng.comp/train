const mongoose = require('../config/database');
const Schema = mongoose.Schema
const postSchema = new Schema({
    titulo: String,
    subtitulo: String,
    user: {
        type: mongoose.Schema.Types.ObjectId, ref: 'User'
    }
},{
    timestamps:true
})

module.exports = mongoose.model('Post', postSchema);