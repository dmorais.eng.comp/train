const userModel = require('./models/User')
const postModel = require('./models/Post')
async function main(){
    const user = await userModel.create({
        nome: 'Diozefe',
        idade: 23,
    })

    const post = await postModel.create({
        titulo: 'Desenvolvimento',
        subtitulo: 'Solução de relacionamento para mongodb',
        user: user._id
    })

    await post.save()

    const userById = await userModel.findById(user._id)

    userById.posts.push(post)
    await userById.save()

    const userByPost = await postModel.findById(post._id).populate('user')
    console.log({
        user,
        post,
        populate: userByPost
    })
}
main()