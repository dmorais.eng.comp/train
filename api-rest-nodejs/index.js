const customExpress =require('./config/customExpress')
const connection = require('./infraestrutura/database/connection')
const Tables = require('./infraestrutura/database/tables')



connection.connect(error=>{
    if(error){
        console.log(error)
    }else{
        console.log('connected database sucessfull')
        //verificar tabelas
        Tables.init(connection)
        //Subir servidor
        const app = customExpress();
        const port = 3000;
        app.listen(port, ()=>{console.log(`Servidor online | Port: ${port}`)})
    }
})
