const fs = require('fs')
const path = require('path')
module.exports = (caminho, nomeArquivo, callbackImagemCriada)=>{
    
    const tipoEsperado = ['jpg','png','jpeg']
    const tipo = path.extname(caminho)

    const tipoValido = tipoEsperado.indexOf(tipo.substring(1)) !== -1

    if (tipoValido) {
        const novoCaminho = `./assets/image/${nomeArquivo}${tipo}`
        fs.createReadStream(caminho)
            .pipe(fs.createWriteStream(novoCaminho))
            .on('finish', ()=>callbackImagemCriada(false, novoCaminho))
    } else {
        const erro = 'Tipo de arquivo esperado é inválido'
        console.log('Erro te tipo de arquivo')
        callbackImagemCriada(erro)
    }

}
