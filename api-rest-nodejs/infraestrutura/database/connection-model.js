const mysql = require('mysql')

const connection = mysql.createConnection({
    host:'your-host',
    port:'3306',
    user:'your-user',
    password:'your-password',
    database:'database-name'
})
module.exports = connection