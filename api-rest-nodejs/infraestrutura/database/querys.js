const connection = require('./connection')
/**?
 * @param query Comando SQL para ser executado
 * @param params Valores que deseja enviar para ser adicionado a query
 */ 
const exeQuery = (query, params='')=>{
    return new Promise((resolve, reject)=>{
        connection.query(query, params, (error, results, field)=>{
            if (error) {
                reject(error)
            } else {
                resolve(results)
            }
        })
    })
}

module.exports = exeQuery