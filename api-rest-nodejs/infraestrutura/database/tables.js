
class Tables {
    /**?
     * @param connection Espera uma conexão que está sendo utilizada
     */
    init(connection){
        this.connection = connection

        this.criarAtendimentos()
        this.criarPets()
    }

    criarAtendimentos(){
        const sql = 'CREATE TABLE IF NOT EXISTS atendimentos '+ 
        '(id int NOT NULL AUTO_INCREMENT, cliente varchar(11) NOT NULL, pet varchar(20), '+
        'servico varchar(20) NOT NULL, status varchar(20) NOT NULL, observacoes text, '+
        'data datetime NOT NULL, dataCriacao datetime NOT NULL, '+
        'PRIMARY KEY(id))'
        
        this.connection.query(sql, (erro)=>{
            if (erro) {
                console.log(erro)
            } else {
                console.log('Tabela ATENDIMENTOS criada com sucesso.')
            }
        })
    }
    criarPets(){
        const sql = 'CREATE TABLE IF NOT EXISTS pets '+
                    '(id int NOT NULL AUTO_INCREMENT, '+
                    'nome varchar(50), imagem varchar(200), '+
                    'PRIMARY KEY(id))';
        this.connection.query(sql, erro=>{
            if (erro) {
                console.log(erro)
            } else {
                console.log('Tabela PETS criada com sucesso.')
            }
        })
    }
}

module.exports = new Tables