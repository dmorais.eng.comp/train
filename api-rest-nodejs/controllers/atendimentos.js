const atendimento_ml = require('../models/atendimentos')
const axios = require('axios')

module.exports= app =>{
    app.post('/atendimentos',(req, res)=>{
        const atendimento = req.body
        atendimento_ml.criar(atendimento)
            .then(atendimentoCadastrado => res.status(201).json(atendimentoCadastrado))
            .catch(erros => res.status(400).json(erros))
    })
    app.get('/atendimentos',(req, res)=>{
        atendimento_ml.lista()
            .then(resultados => res.json(resultados))
            .catch(erros => res.status(400).json(erros))
    })
    app.get('/atendimentos/:id', (req,res)=>{
        const id = parseInt(req.params.id)

        atendimento_ml.buscaId(id)
            .then(async resultados=>{
                const atendimento = resultados[0]
                const cpf = atendimento.cliente
                const { data } = await axios.get(`http://localhost:8082/${cpf}`)
                atendimento.cliente.dados = data
                res.status(200).json(atendimento)
            })
            .catch(erros => res.status(400).json(erros))

    })
    app.patch('/atendimentos/:id', (req,res)=>{
        const id = parseInt(req.params.id)
        const valores = req.body
        atendimento_ml.alterar(id, valores)
            .then(valoresAtualizados=> res.status(200).json(valoresAtualizados))
            .catch(erros=>res.status(400).json(erros))
    })
    app.delete('/atendimentos/:id', (req, res)=>{
        const id = parseInt(req.params.id)
        atendimento_ml.deletar(id)
            .then(resultados=>res.status(200).json({id}))
            .catch(erros=>res.status(400).json(erros))
    })
    
}