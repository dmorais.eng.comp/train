const conn = require('../infraestrutura/database/connection')
const uploadDeArquivo = require('../infraestrutura/arquivos/uploadArquivo')
class Pets {
    criar(pet, res){
        
        const sql = 'INSERT INTO pets SET ?'

        uploadDeArquivo(pet.imagem, pet.nome, (erro, novoCaminho)=>{
            if (erro) {
                res.status(400).json({erro})
            } else {
                const novoPet = {nome: pet.nome, imagem: novoCaminho}
                conn.query(sql, novoPet, erro=>{
                    if (erro) {
                        res.status(400).json(erro)
                    } else {
                        res.status(200).json(novoPet)
                    }
                })
            }
        })
    }
}

module.exports = new Pets