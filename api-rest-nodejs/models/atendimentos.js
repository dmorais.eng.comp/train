const moment = require('moment')
const repositorio = require('../repositorios/atendimento')

class Atendimentos {
    constructor(){
        //Validação de dados
        this.dataValida = ({data, dataCriacao}) => moment(data).isSameOrAfter(dataCriacao)
        this.clienteValido = (tamanho) => tamanho >= 4
        this.valida = parametros => this.validacoes.filter(campo=>{
            const {nome}=campo
            const parametro = parametros[nome]
            return !campo.valido(parametro)
        })
        this.validacoes = [
            {
                nome:'data',
                valido: this.dataValida,
                mensagem:'Data deve ser maior ou igual a data atual'
            },
            {
                nome:'cliente',
                valido: this.clienteValido,
                mensagem:'O nome deve ter pelo menos 4 caracteres'
            }
        ]
    }
    criar(atendimento){
        //Manipulação e criação de data
        const dataCriacao = moment().format('YYYY-MM-DD HH:MM:SS')
        const data = moment(atendimento.data, 'DD/MM/YYYY').format('YYYY-MM-DD HH:MM:SS')
        
        const parametros = {
            data: {data, dataCriacao},
            cliente: { tamanho: atendimento.cliente.length }
        }
        
        const erros = this.valida(parametros)
        const existemErros = erros.length

        //Pos-Validação
        if(existemErros){
            console.log(this.validacoes[0].valido(parametros.data), this.validacoes[1].valido(parametros.cliente))
            return new Promise((resolve, reject)=>reject(erros))
        }else{
            const atendimentoDatado = {...atendimento, dataCriacao, data}
            return repositorio.adiciona(atendimentoDatado)
                .then(resultados=>{
                    const id = resultados.insertId
                    return {...atendimentoDatado, id}
                })
        }

    }
    lista(){
        return repositorio.lista()
    }
    buscaId(id){
        return repositorio.buscaId(id)
    }
    alterar(id, valores){
        if (valores.data) {
            valores.data = moment(valores.data, 'DD/MM/YYYY').format('YYYY-MM-DD HH:MM:SS')
        }
        const parametros = {data: valores.data}
        const erros = this.valida(parametros)
        const existemErros = erros.length
        if(existemErros){
            return new Promise((resolve, reject)=>reject(erros))
        }else{
            return repositorio.alterar(id, valores)
                .then(resultados=>{
                    const id = resultados.insertId
                    return {...valores, id}
                })
        }
    }
    deletar(id){
        return repositorio.deletar(id)
    }
}

module.exports = new Atendimentos