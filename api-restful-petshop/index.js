const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const config = require('config')

app.use(bodyParser.json())

const roteador = require('./api/rotas/fornecedores')
const NaoEncontrado = require('./api/error/NaoEncontado')
const CampoInvalido = require('./api/error/CampoInvalido')
const DadosNaoFornecidos = require('./api/error/DadosNaoFornecidos')
const ValorNaoSuportado = require('./api/error/ValorNaoSuportado')


app.use('/api/fornecedores', roteador)
app.use((error, req, res, next)=>{
    let status = 500
    if(error instanceof NaoEncontrado){
        status = 404
    }
    if (error instanceof CampoInvalido || error instanceof DadosNaoFornecidos) {
        status = 400
    }
    if (error instanceof ValorNaoSuportado) {
        status = 406
    }
    res.status(status)
    res.json({  error:error.message, 
                id: error.idError
            })
})


app.listen(config.get('api.port'), () => console.log('A API está funcionando!'))