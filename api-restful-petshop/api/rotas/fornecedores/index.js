const roteador = require('express').Router()
const TabelaFornecedor = require('./TabelaFornecedor')
const Fornecedor = require('./Fornecedor')
const NaoEncontrado = require('../../error/NaoEncontado')

roteador.get('/', async (requisicao, resposta) => {
    const resultados = await TabelaFornecedor.listar()
    resposta.status(200).json(resultados)
})
roteador.post('/', async (req, res, next)=>{
        try {
            const dadosRecebidos = req.body
            const fornecedor = new Fornecedor(dadosRecebidos)
            await fornecedor.criar()
            
            res.status(201).json(fornecedor)
        } catch (error) {
           next(error)
        }
        
})
roteador.get('/:id',async (req, res, next)=>{
    try {
        const id = req.params.id
        const fornecedor = new Fornecedor({ id:id })
        await fornecedor.carregar()
        res.status(200).json(fornecedor)
    } catch (error) {
        next(error)
    }
})
roteador.put('/:id', async (req, res, next)=>{
    try {
        const id = req.params.id
        const dadosRecebidos = req.body
        const dados = Object.assign({},dadosRecebidos,{ id:id })
        const fonecedor = new Fornecedor(dados)
        await fonecedor.atualizar()
        res.status(204).end()
        
    } catch (error) {
        next(error)
    }
})
roteador.delete('/:id', async (req, res, next)=>{
    try {
        const id = req.params.id
        const fornecedor = new Fornecedor({ id:id })
        await fornecedor.carregar()
        await fornecedor.remover()
        res.status(204).end()
    } catch (error) {
        next(error)
    }
})
module.exports = roteador