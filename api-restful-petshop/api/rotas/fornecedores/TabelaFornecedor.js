const NaoEncontrado = require('../../error/NaoEncontado')
const Modelo = require('./ModeloTabelaFornecedor')

module.exports = {
    listar () {
        return Modelo.findAll()
    },
    inserir(fornecedor){
        return Modelo.create(fornecedor)
    },
    async pegarPorId(id){
        const encontrado = await Modelo.findOne({
            where:{ id:id }
        })
        if(!encontrado){
            throw new NaoEncontrado()
        }
        return encontrado
    },
    atualizar(id, dadosParaAtualizar){
        return Modelo.update(
            dadosParaAtualizar,
            { where: { id:id } }
        )
    },
    async remover(id) {
        const encontrado = await Modelo.findOne({where: { id: id }})
        if (!encontrado) {
            throw new NaoEncontrado()
        }
        Modelo.destroy({
            where:{ id: id }
        })
    }
}