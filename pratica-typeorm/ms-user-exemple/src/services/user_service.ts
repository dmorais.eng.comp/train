import {UserRequest, UserResponse } from "../models/user";
import { User } from "../entity/User";
import { Connection, createConnection } from "typeorm";
import { isParameter } from "typescript";
import  Errors  from '../error/verify_data'

const grpc = require('@grpc/grpc-js');

function errorResponse(msg: string, callback: (err: any, resp: any) => any): any {
	console.log(msg);
	return callback({
    	code: 400,
    	message: msg,
    	status: grpc.status.INTERNAL
	}, null);
}

let connection: Connection;
async function getConnection(callback: (err: any, resp: any) => any): Promise<Connection> {
	try {
    	if (connection && connection.isConnected) {
        	return connection;
    	}
    	connection = await createConnection();
    	return connection;
	} catch (err: any) {
    	return errorResponse(`Error while connecting: [${err}]`, callback);
	}
}

async function handleCreate(call: any, callback: (err: any, resp: any) => any) {
	//Verifica erros antes de continuar
	await Errors.verify(call.request, callback)

	getConnection(callback).then(async connection => {
    	try {
			if(!call.request){
				return errorResponse(`Nenhum campo obrigatório foi passado, impossível continuar`, callback);
			}
        	const userRepository = await connection.getRepository<User>(User);
        	const userParam: UserRequest = call.request;
    	
       	const toSaveUser: User = {
           	name: userParam.name,
           	email: userParam.email,
           	password: userParam.password,
		}

		await userRepository.save(toSaveUser);
		const userSaved = await userRepository.find({email: userParam.email});

		const token = 'ASDASDJAJSD1872E1EG171223DKND1P9';
		delete userSaved[0].password;
		
		const finalUser: UserResponse = {...userSaved[0], token};
        	return callback(null, finalUser);
    	} catch (err: any) {
        	return Errors.verify(call.request, callback)
    	}
	});
}
export { handleCreate }
