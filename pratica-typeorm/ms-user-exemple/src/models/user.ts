import { ObjectId } from "mongodb"

type UserRequest={
	name: string;
	email: string;
	password: string;
}
type UserResponse={
	id?: ObjectId;
	name: string;
	email: string;
	token: string;
}

export { UserRequest, UserResponse }
