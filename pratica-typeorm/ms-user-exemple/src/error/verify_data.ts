const grpc = require('@grpc/grpc-js');
import { User } from "../entity/User";
import { Connection, createConnection } from "typeorm";

export default {
    verify(value: any, callback: (err: any, resp: any) => any):any{
        verifyUserExists(value, callback);
        verifyObjectValues(value, callback);
    }

}
let connection: Connection;
async function getConnection(callback: (err: any, resp: any) => any): Promise<Connection> {
	try {
    	if (connection && connection.isConnected) {
        	return connection;
    	}
    	connection = await createConnection();
    	return connection;
	} catch (err: any) {
    	return errorResponse(`Error while connecting: [${err}]`, callback);
	}
}

function verifyUserExists(value: any, callback: (err: any, resp: any) => any): void{
    getConnection(callback).then(async connection => {
        const userRepository = await connection.getRepository<User>(User);
        const user = await userRepository.findOne({email: value.email});
        if(user){
            return errorResponse(`O usuário de email ${value.email} já existe`, callback);
        }
    });
}

function verifyObjectValues(value: any, callback: (err: any, resp: any) => any):any {
    let values = Object.entries(value)
		for(let i=0;i<values.length;i++){
			for(let x=0; x<values[i].length;x++){
				if(values[i][x] == '' || values[i][x] ==undefined || values[i][x] == null){
					return errorResponse(`O campo ${values[i][0]} é obrigatório`, callback);
				}
			}
		}
}

function errorResponse(msg: string, callback: (err: any, resp: any) => any): any {
	return callback({
    	code: 400,
    	message: msg,
    	status: grpc.status.INTERNAL
	}, null);
}