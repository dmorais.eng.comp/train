import "reflect-metadata";
import {
	handleCreate
} from './services/user_service';

const PROTO_PATH = __dirname + '/proto/user.proto';
const grpc = require('@grpc/grpc-js');
const protoLoader = require('@grpc/proto-loader');

let packageDefinition = protoLoader.loadSync(
	PROTO_PATH,
	{
    	keepCase: true,
    	longs: String,
    	enums: String,
    	defaults: true,
    	oneofs: true
	});
	let userProto = grpc.loadPackageDefinition(packageDefinition).userPackage;
	
	async function main() {
	let server = new grpc.Server();
	server.addService(userProto.UserService.service, {
   	create: handleCreate
	});

	server.bindAsync('0.0.0.0:50051', grpc.ServerCredentials.createInsecure(), () => {
    	server.start();
    	console.log('ms-user microservice started. Listening at 127.0.0.1:50051');
	});
}

if (require.main === module) {
	main();
}
