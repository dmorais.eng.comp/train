const grpc = require('@grpc/grpc-js');
const protoLoader = require('@grpc/proto-loader');

const clients = new Map<string, any>();

function getGrpcClient(protoFileName: string, protoPackageName: string, serviceName: string, address: string = 'localhost:50051') {
    if (clients.has(protoFileName)) {
        return clients.get(protoFileName);
    }

    const PROTO_PATH = __dirname + `/proto/${protoFileName}.proto`;
    const packageDefinition = protoLoader.loadSync(
        PROTO_PATH,
        {
            keepCase: true,
            longs: String,
            enums: String,
            defaults: true,
            oneofs: true
        });

    const proto = grpc.loadPackageDefinition(packageDefinition)[protoPackageName];
    const client = new proto[serviceName](`${address}`, grpc.credentials.createInsecure());
    clients.set(protoFileName, client);

    return client;
}

export { getGrpcClient }
