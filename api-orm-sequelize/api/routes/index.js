const {json, urlencoded} = require('body-parser')
const pessoas =  require('./pessoasRoutes')
const niveis = require('./niveisRoute')
const turmas = require('./turmasRoute')
module.exports = app =>{
    app.use(
        urlencoded({extended:true}),
        json()
    )
    app.use(
        pessoas,
        niveis,
        turmas
    )
    

    
}